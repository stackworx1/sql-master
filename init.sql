create table personnel_whereabouts as
select id,
       md5(random()::text) as uuid,
       floor(random() * 9 + 1) as personnel_id,
       floor(random() * 9 + 1) as place_id,
       (array[null, floor(random() * 10 + 1) ])[floor(random() * 2 + 1)] as backup_1_place_id,
       (array[null, floor(random() * 10 + 1) ])[floor(random() * 2 + 1)] as backup_2_place_id,
       (array[True, False])[floor(random() * 2 + 1)] as point_of_interest,
       NOW() - (random() * (NOW()+'90 days' - NOW())) as timestamp
from generate_Series(1, 150000) id;


create table personnel as
select id,
       md5(random()::text) as name,
       (array['MethodA', 'MethodB', 'MethodC'])[floor(random() * 3 + 1)] as method,
       NOW() - (random() * (NOW()+'90 days' - NOW())) as joined_date
from generate_Series(1, 10) id;

create table if not exists place as
select id,
       md5(random()::text) as address,
       random() * 2.0000 + -25.000 as latitude,
       random() * 2 + 15 as longitude,
       (array[True, False])[floor(random() * 2 + 1)] as vendor_of_interest,
       (array['MethodA', 'MethodB', 'MethodC'])[floor(random() * 3 + 1)] as method,
        NOW() - (random() * (NOW()+'90 days' - NOW())) as created_at
from generate_Series(1, 10) id;