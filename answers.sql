-- R1:
select p.name
from public.personnel p inner join (
  select id
    from place
    group by id
    order by count(*) desc 
fetch first row
only
) p2
on p2.id = p.id
--end of R1

-- R2:
SELECT pw.place_id AS "Place id",
    p.name AS "Name"
FROM personnel_whereabouts pw , personnel p
WHERE pw.personnel_id = p.id
    AND pw.place_id =
     (
       SELECT MAX(pw.place_id)
    FROM personnel_whereabouts pw
    WHERE pw.personnel_id = p.id
COUNT
(pw.place_id)
     );

-- R3: 
SELECT CASE WHEN EXISTS (
    SELECT pw.point_of_interest
    FROM personnel_whereabouts pw
    WHERE pw.point_of_interest = true 
)
THEN CAST(1 AS BIT)
ELSE CAST(0 AS BIT) END

-- R4:


-- R5:
alter table personnel_whereabouts
add foreign key (personnel_id) references personnel(personnel
.id)

add foreign key
(place_id) references place
(place.id)
add foreign key
(backup_1_place_id) references place
(place.id)
add foreign key
(backup_2_place_id) references place
(place.id)


-- R6:


-- R7:
select *
from personnel_whereabouts pw inner join place p
    on pw.id = p.id, SQRT(
    POW(69.1 * (p.latitude - (-25.7477)), 2) +
    POW(69.1 * (28.2384 - p.longitude ) * COS(p.latitude / 57.3), 2)) as distance
from personnel_whereabouts pw inner join place p
     on pw.id = p.id,
having distance < 500 order by distance;
