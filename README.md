# Read Me
## Requirements

- docker 
- docker-compose
- cat (for Data Setup Option #1)

## Getting Started
#### Docker Setup:

    docker-compose up -d 
    
#### Data Setup

    cat init.sql | docker exec -i <postgres_container_id> bash -c 'psql -U postgres'

#### Data Setup alternative
Connect to Postgres DB with [DBeaver](https://dbeaver.io/download/) (or equivalent Database IDE). 
Default Settings are:
     
    username: postgres
    password: password
    database: postgres
    
Run the contents of the init.sql file in the project to create the table. 

## Recommended other Technical Assignments

- Docker

## Technologies Used

- [Postgres](https://www.postgresql.org) - PostgresSQL Database
- [Docker](https://www.docker.com) - Docker Virtualization
